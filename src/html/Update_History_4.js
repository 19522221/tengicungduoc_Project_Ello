import React from 'react';
import { Link } from 'react-router-dom';


function Update_History_4() {
    return (
    <html>

        <head>
        <link href="https://fonts.googleapis.com/css?family=ZCOOL+QingKe+HuangYou&display=swap" rel="stylesheet" />
        <link href="../css/Update History 4.css" rel="stylesheet" />
        <title>Document</title>
        </head>

        <body>
        <div class="v21_5">
            <div class="v21_124"><span class="v21_125">Choose a quarantine facility</span>
                <div class="v21_126">
                    <div class="v21_127"><span class="v21_128">General isolation base set</span>
                        <div class="v21_129"></div>
                    </div>
                </div>
                <div class="v21_130">
                    <div class="v21_131"><span class="v21_132">Self-selected quarantine facility</span>
                        <div class="v21_133"></div>
                    </div>
                </div>
                <div class="v21_134">
                    <div class="v21_135"><span class="v21_136">Other</span>
                        <div class="v21_137"></div>
                    </div>
                </div>
            </div>
            <div class="v21_139">
                <div class="v21_140"></div><span ><Link to='/Epidemic_situation_mid_page' class="v21_141">Update</Link></span>
            </div>
        </div>
        </body>

    </html>
    )
}

export default Update_History_4;