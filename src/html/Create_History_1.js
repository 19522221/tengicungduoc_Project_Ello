import React from 'react';
import { Link } from 'react-router-dom';


function Create_History_1() {
    return (
        <html>

        <head>
            <link href="https://fonts.googleapis.com/css?family=ZCOOL+QingKe+HuangYou&display=swap" rel="stylesheet" />
            <link href="../css/Create History 1.css" rel="stylesheet" />
            <title>Document</title>
        </head>

        <body>
            <div class="v5_72">
                <div class="v52_0"><span class="v5_73">History</span>
                    <div class="v5_74"></div>
                    <div class="v5_75"></div>
                </div>
                <div class="v5_101"><span class="v5_81">Gate</span>
                    <div class="v5_87"></div>
                </div>
                <div class="v5_104"><span class="v5_85">Nationality</span>
                    <div class="v5_88"></div>
                </div>
                <div class="v5_103"><span class="v5_83">Date of birth</span>
                    <div class="v5_89"></div>
                </div>
                <div class="v5_105"><span class="v5_84">Gender</span>
                    <div class="v5_90"></div>
                </div>
                <div class="v5_106"><span class="v5_86">Passport number / other legal document</span>
                    <div class="v5_92"></div>
                </div>
                <div class="v5_102"><span class="v5_82">Full name</span>
                    <div class="v5_91"></div>
                </div>
                
                <div class="v45_13">
                    <div class="v45_10"><span ><Link to='/Create_History_2' class="v45_11">Next</Link></span></div>
                </div>
            </div>
        </body>

        </html>
    )
}

export default Create_History_1;