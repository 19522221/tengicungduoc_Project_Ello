import React from 'react';
import { Link } from 'react-router-dom';


function Reset_Password() {
    return (
    <html>

        <head>
            <link href="https://fonts.googleapis.com/css?family=ZCOOL+QingKe+HuangYou&display=swap" rel="stylesheet" />
            <link href="../css/Reset Password.css" rel="stylesheet" />
            <title>Document</title>
        </head>

        <body>
            <div class="v31_49">
                <div class="v31_50"></div>
                <div class="v31_51"></div><span class="v31_52">Forgot password</span>
                <div class="v31_54">
                    <div class="v31_55"></div><span class="v31_56">New Password</span>
                </div>
                <div class="v31_57">
                    <div class="v31_58"></div><span class="v31_59">Confirm Password</span>
                </div>
                <div class="v31_60">
                    <div class="v31_61"></div><span ><Link to='/Sign_in' class="v31_62">Reset Password</Link></span>
                </div>
            </div>
        </body>

    </html>

    )
}

export default Reset_Password;