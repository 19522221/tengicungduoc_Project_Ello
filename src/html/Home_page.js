import React from 'react';
import { Link } from 'react-router-dom';

function Home_page() {
    return (
        <html>

            <head>
                <link href="https://fonts.googleapis.com/css?family=ZCOOL+QingKe+HuangYou&display=swap" rel="stylesheet" />
                <link href="https://fonts.googleapis.com/css?family=Special+Elite&display=swap" rel="stylesheet" />
                <link href="../css/Home_page.css" rel="stylesheet" />
                <title>Document</title>
            </head>

            <body>
                <div class="v19_16">
                    <Link to='/Sign_in' class="v1">Sign in</Link>
                    <Link to='/Sign_up' class="v2">Sign up</Link>
                    <div class="group">
                        <div class="v5_26">ELLO</div>
                        <div class="v5_27">Join hands to protect public health</div>
                       
                    </div>
                </div>
            </body>

        </html>
    )
}

export default Home_page;