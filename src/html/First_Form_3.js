import React from 'react';
import { Link } from 'react-router-dom';


function First_Form_3() {
    return (
        <html>

        <head>
            <link href="https://fonts.googleapis.com/css?family=ZCOOL+QingKe+HuangYou&display=swap" rel="stylesheet" />
            <link href="../css/First Form 3.css" rel="stylesheet" />
            <title>Document</title>
        </head>

        <body>
            <div class="v21_191">
                <div class="v31_13">
                    <div class="v21_182"><span class="v21_183">Immigration date</span>
                        <div class="v21_184"></div>
                    </div>
                    <div class="v21_179"><span class="v21_183">Place of departure</span>
                        <div class="v21_184"></div>
                    </div>
                    <div class="v21_176"><span class="v21_183">Accommodation after concentrated isolation</span>
                        <div class="v21_184"></div>
                    </div>
                    <div class="v31_2"><span class="v21_193">Contact information in Viet Nam</span>
                        <div class="v21_194">
                            <div class="v21_195"></div><span class="v21_196">Address</span>
                        </div>
                        <div class="v21_197">
                            <div class="v21_195"></div><span class="v21_196">Phone</span>
                        </div>
                        <div class="v21_200">
                            <div class="v21_195"></div><span class="v21_196">Email</span>
                        </div>
                    </div>
                </div>
                <div class="v45_41">
                    <div class="v45_42"></div><span><Link to='/First_Form_4' class="v45_43">Next</Link></span>
                </div>
            </div>
        </body>

        </html>
    )
}

export default First_Form_3;