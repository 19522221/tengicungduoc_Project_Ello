import React from 'react';
import { Link } from 'react-router-dom';


function First_Form_5() {
    return (
        <html>

        <head>
            <link href="https://fonts.googleapis.com/css?family=ZCOOL+QingKe+HuangYou&display=swap" rel="stylesheet" />
            <link href="../css/First Form 5.css" rel="stylesheet" />
            <title>Document</title>
        </head>

        <body>
            <div class="v31_6">
                <div class="v31_5"><span class="v21_231">Choose a quarantine facility</span>
                    <div class="v21_232"><span class="v21_234">General isolation base set</span>
                        <div class="v21_235"></div>
                    </div>
                    <div class="v21_236"><span class="v21_234">Self-selected quarantine facility</span>
                        <div class="v21_235"></div>
                    </div>
                    <div class="v21_240"><span class="v21_234">Other</span>
                        <div class="v21_235"></div>
                    </div>
                </div>
                <div class="v31_17">
                    <div class="v31_18"></div><span ><Link to='/Sign_in' class="v31_19">Submit</Link></span>
                </div>
            </div>
        </body>

        </html>
    )
}

export default First_Form_5;