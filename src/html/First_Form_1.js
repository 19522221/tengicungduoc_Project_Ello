import React from 'react';
import { Link } from 'react-router-dom';


function First_Form_1() {
    return (
        <html>

        <head>
            <link href="https://fonts.googleapis.com/css?family=ZCOOL+QingKe+HuangYou&display=swap" rel="stylesheet" />
            <link href="../css/First Form 1.css" rel="stylesheet" />
            <title>Document</title>
        </head>

        <body>
            <div class="v21_147">
                <div class="table">
                    <div class="v21_151">
                        <span class="v21_152">Gate</span>
                        <div class="v21_153"></div>
                    </div>
                    <div class="v21_166">
                        <span class="v21_152">Full name</span>
                        <div class="v21_153"></div>
                    </div>
                    <div class="v21_157">
                        <span class="v21_152">Date of birth</span>
                        <div class="v21_153"></div>
                    </div>
                    <div class="v21_154">
                        <span class="v21_152">Nationality</span>
                        <div class="v21_153"></div>
                    </div>
                    <div class="v21_160">
                        <span class="v21_152">Gender</span>
                        <div class="v21_153"></div>
                    </div>
                </div>

                <div class="v31_9">
                    <span class="v21_270">Form</span>
                    <div class="v21_271"></div>
                    <div class="v21_272"></div>
                </div>

                <div class="v45_47">
                    <div class="v45_48"></div>
                    <span><Link to='/First_Form_2'  class="v45_49">Next</Link></span>
                </div>
            </div>

        </body>

        </html>
    )
}

export default First_Form_1;