import React from 'react';
import { Link } from 'react-router-dom';


function Update_History_2() {
    return (
    <html>

        <head>
        <link href="https://fonts.googleapis.com/css?family=ZCOOL+QingKe+HuangYou&display=swap" rel="stylesheet" />
        <link href="../css/Update History 2.css" rel="stylesheet" />
        <title>Document</title>
        </head>

        <body>
        <div class="v21_3">
            <div class="v21_21"><span class="v21_22">Accommodation after concentrated isolation</span>
                <div class="v21_23"></div>
            </div>
            <div class="v21_24"><span class="v21_25">Place of departure</span>
                <div class="v21_26"></div>
            </div>
            <div class="v21_37"><span class="v21_38">Contact information in Viet Nam</span>
                <div class="v21_39">
                    <div class="v21_40"></div><span class="v21_41">Address</span>
                </div>
                <div class="v21_42"><span class="v21_43">Phone</span>
                    <div class="v21_44"></div>
                </div>
                <div class="v21_45"><span class="v21_46">Email</span>
                    <div class="v21_47"></div>
                </div>
            </div>
            <div class="v45_26">
                <div class="v45_27"></div><span><Link to='/Update_History_3'  class="v45_28">Next</Link></span>
            </div>
        </div>
        </body>

    </html>

    )
}

export default Update_History_2;