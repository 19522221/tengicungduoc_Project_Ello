import { Route, Link } from 'react-router-dom';
import Home_page from './html/Home_page';
import Sign_in from './html/Sign_in';
import Sign_up from './html/Sign_up';
import Create_History_1 from './html/Create_History_1';
import Create_History_2 from './html/Create_History_2';
import Create_History_3 from './html/Create_History_3';
import Create_History_4 from './html/Create_History_4';
import Update_History_1 from './html/Update_History_1';
import Update_History_2 from './html/Update_History_2';
import Update_History_3 from './html/Update_History_3';
import Update_History_4 from './html/Update_History_4';
import First_Form_1 from './html/First_Form_1';
import First_Form_2 from './html/First_Form_2';
import First_Form_3 from './html/First_Form_3';
import First_Form_4 from './html/First_Form_4';
import First_Form_5 from './html/First_Form_5';
import Epidemic_situation_map from './html/Epidemic_situation_map';
import Epidemic_situation_mid_page from './html/Epidemic_situation_mid_page';
import Logged from './html/Logged';
import Reset_Password from './html/Reset_Password';


function Route_() {
  return (
    <div className="Route_">
      <Route exact path='/' component={Home_page} />
      <Route exact path='/Sign_in' component={Sign_in} />
      <Route exact path='/Sign_up' component={Sign_up} />
      <Route exact path='/Create_History_1' component={Create_History_1} />
      <Route exact path='/Create_History_2' component={Create_History_2} />
      <Route exact path='/Create_History_3' component={Create_History_3} />
      <Route exact path='/Create_History_4' component={Create_History_4} />
      <Route exact path='/Update_History_1' component={Update_History_1} />
      <Route exact path='/Update_History_2' component={Update_History_2} />
      <Route exact path='/Update_History_3' component={Update_History_3} />
      <Route exact path='/Update_History_4' component={Update_History_4} />
      <Route exact path='/First_Form_1' component={First_Form_1} />
      <Route exact path='/First_Form_2' component={First_Form_2} />
      <Route exact path='/First_Form_3' component={First_Form_3} />
      <Route exact path='/First_Form_4' component={First_Form_4} />
      <Route exact path='/First_Form_5' component={First_Form_5} />
      <Route exact path='/Epidemic_situation_mid_page' component={Epidemic_situation_mid_page} />
      <Route exact path='/Epidemic_situation_map' component={Epidemic_situation_map} />
      <Route exact path='/Logged' component={Logged} />
      <Route exact path='/Reset_Password' component={Reset_Password} />
      
      
      
    </div>
  );
}

export default Route_;
